import datetime
from django.contrib import admin

from records.models import Doctor, RecordTime

from django.utils.translation import ugettext_lazy as _


class RecordTimeListFilter(admin.SimpleListFilter):
    title = _('Записи')
    parameter_name = 'record_datetime'

    def lookups(self, request, model_admin):
        return (
            ('active', _('Активные заявки')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            return queryset.filter(
                record_datetime__gte=datetime.datetime.now().strftime('%Y-%m-%d %I:%M'),
            )


class RecordTimeAdmin(admin.ModelAdmin):
    list_filter = (RecordTimeListFilter,)
    list_display = ('doctor',  'name', 'record_datetime',)
    date_hierarchy = 'record_datetime'


admin.site.register(Doctor)
admin.site.register(RecordTime, RecordTimeAdmin)
