import datetime
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from formtools.wizard.views import SessionWizardView
from doctor_records.settings import SCHEDULE_DAYS

from records.models import RecordTime
from records.schedule import Schedule


class ContactWizard(SessionWizardView):
    template_name = 'form.html'

    def done(self, form_list, **kwargs):
        instance = RecordTime()
        for form in form_list:
            for field, value in form.cleaned_data.items():
                setattr(instance, field, value)
        instance.save()
        return HttpResponseRedirect(reverse_lazy('success'))

    def get_form_initial(self, step):
        if int(step) == 1:
            doctor = self.get_cleaned_data_for_step('0')['doctor']
            # минимальное время для записи + 2часа
            date = datetime.datetime.now() + datetime.timedelta(hours=2)
            # Создаем список с зарезервированным временем у выбранного объекта доктор
            busy_schedule = [time.record_datetime for time in RecordTime.objects.filter(doctor_id=doctor.pk)]
            # Создаем доступное расписание для выбранного объекта доктор на количество дней указанных в сеттингах
            #  с текущей даты
            available_schedule = Schedule.available_schedule(busy_schedule, SCHEDULE_DAYS, date)
            self.initial_dict['1'] = {
                'record_datetime': available_schedule
            }
        return super().get_form_initial(step)


class SuccessView(TemplateView):
    template_name = 'success.html'
