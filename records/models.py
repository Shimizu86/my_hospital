from django.db import models


class Doctor(models.Model):
    fio = models.CharField(max_length=200)
    profession = models.CharField(max_length=200)

    def __str__(self):
        return self.fio + ' ('+self.profession+')'


class RecordTime(models.Model):
    doctor = models.ForeignKey(Doctor, verbose_name='Врач')
    record_datetime = models.DateTimeField()
    name = models.CharField(max_length=200, verbose_name='ФИО')

    def __str__(self):
        return str(self.record_datetime)
