# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('fio', models.CharField(max_length=200)),
                ('profession', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='RecordTime',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('record_datetime', models.DateTimeField()),
                ('name', models.CharField(max_length=200, verbose_name='ФИО')),
                ('doctor', models.ForeignKey(verbose_name='Врач', to='records.Doctor')),
            ],
        ),
    ]
