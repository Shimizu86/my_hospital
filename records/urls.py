from django.conf.urls import url
from records.forms import SelectDoctorForm1, SelectDoctorForm2

from records.views import ContactWizard, SuccessView

urlpatterns = [
    url(r'^new_record/$',
        ContactWizard.as_view([SelectDoctorForm1, SelectDoctorForm2]),
        name='new_record'),
    url(r'^success/$', SuccessView.as_view(), name='success')
]
