import datetime


class Schedule:
    @classmethod
    def _create_schedule(cls, date, days):
        schedule = []
        max_schedule_date = date + datetime.timedelta(days)
        # условие для формирования расписание (интервал 1 час)
        while date < max_schedule_date:
            # проверка текущей даты на соответствие условию - будний день,
            #  время с 9 до 18-00
            if date.weekday() < 5 and (9 <= date.hour < 19):
                new_date = datetime.datetime(
                    date.year,
                    date.month,
                    date.day,
                    date.hour,
                    00,
                    00
                )
                # добавление нового доступного времени для записи
                schedule.append(new_date)
            date = date + datetime.timedelta(hours=1)
        return schedule

    @classmethod
    def available_schedule(cls, busy_schedule, days, date):
        return [(dt, dt) for dt in Schedule._create_schedule(date, days) if dt not in busy_schedule]
