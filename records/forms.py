from django import forms
from records.models import RecordTime


class SelectDoctorForm1(forms.ModelForm):
    class Meta(object):
        model = RecordTime
        fields = ('doctor',)


class SelectDoctorForm2(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SelectDoctorForm2, self).__init__(*args, **kwargs)
        initial = kwargs.get('initial')
        available_schedule = initial.get('record_datetime') if initial else []
        self.fields['record_datetime'] = forms.ChoiceField(choices=available_schedule)

    class Meta(object):
        model = RecordTime
        fields = ('name', 'record_datetime',)
        widgets = {
            'record_datetime': forms.Select,
        }
