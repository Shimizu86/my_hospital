import datetime
from django.test import TestCase
from .schedule import Schedule


class AvailableScheduleTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.full_schedule = [
            datetime.datetime(2015, 12, 9, 9, 0),
            datetime.datetime(2015, 12, 9, 10, 0),
            datetime.datetime(2015, 12, 9, 11, 0),
            datetime.datetime(2015, 12, 9, 12, 0),
            datetime.datetime(2015, 12, 9, 13, 0),
            datetime.datetime(2015, 12, 9, 14, 0),
            datetime.datetime(2015, 12, 9, 15, 0),
            datetime.datetime(2015, 12, 9, 16, 0),
            datetime.datetime(2015, 12, 9, 17, 0),
            datetime.datetime(2015, 12, 9, 18, 0)
        ]
        cls.busy_schedule = [
            datetime.datetime(2015, 12, 9, 14, 0),
            datetime.datetime(2015, 12, 9, 10, 0),
            datetime.datetime(2015, 12, 9, 15, 0)
        ]
        cls.result = [
            (datetime.datetime(2015, 12, 9, 9, 0),
             datetime.datetime(2015, 12, 9, 9, 0)),
            (datetime.datetime(2015, 12, 9, 11, 0),
             datetime.datetime(2015, 12, 9, 11, 0)),
            (datetime.datetime(2015, 12, 9, 12, 0),
             datetime.datetime(2015, 12, 9, 12, 0)),
            (datetime.datetime(2015, 12, 9, 13, 0),
             datetime.datetime(2015, 12, 9, 13, 0)),
            (datetime.datetime(2015, 12, 9, 16, 0),
             datetime.datetime(2015, 12, 9, 16, 0)),
            (datetime.datetime(2015, 12, 9, 17, 0),
             datetime.datetime(2015, 12, 9, 17, 0)),
            (datetime.datetime(2015, 12, 9, 18, 0),
             datetime.datetime(2015, 12, 9, 18, 0))
        ]
        cls.date = datetime.datetime(2015, 12, 9)
        cls.days = 1

    def test_full_schedule(self):
        self.assertEqual(Schedule._create_schedule(self.date, self.days), self.full_schedule)

    def test_available_schedule(self):
        self.assertEqual(Schedule.available_schedule(self.busy_schedule, self.days, self.date), self.result)
