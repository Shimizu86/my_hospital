#!/bin/bash

apt-get update
apt-get install -y build-essential python python-dev libmysqlclient-dev libpq-dev python3 python3-pip
pip3 install -r /vagrant/requirements.txt
python3 /vagrant/manage.py migrate
python3 /vagrant/manage.py loaddata doctors.json